function createCard(name, description, pictureUrl, start, end, lname) {
  return `
  <div class="col">
  <div class="card">
    <img src="${pictureUrl}" class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">${name}</h5>
      <h6 class="card-subtitle mb-2 text-muted"> ${lname} </h6>
      <p class="card-text">${description} </p>
      <div class="card-footer">
      <small class="text-muted">${start} - ${end}</small>
    </div>
  </div>
  </div>
  </div>

  `;
}


window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';
  try{
      const response = await fetch(url);
      if(!response.ok){
      // console.log(response);
          throw new Error('Response not ok');
      }else{
          const data = await response.json();

          for (let conference of data.conferences) {
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const starts = new Date( details.conference.starts);
                const date=starts.getDate()
                const month=starts.getMonth()
                const year=starts.getFullYear()
                const start=date+'/'+month+'/'+year
                const ends = new Date (details.conference.ends);
                const edate=ends.getDate()
                  const emonth=ends.getMonth()
                  const eyear=ends.getFullYear()
                  const end= edate+'/'+emonth+'/'+eyear
                  const lname=details.conference.location.name

                  const html = createCard(name, description, pictureUrl, start, end, lname);
                  
                  console.log(html);
                  const column = document.querySelector('.row ');
                    column.innerHTML += html;
                }
    }
}
} catch (error) {
        console.error('error', error);
      }
});

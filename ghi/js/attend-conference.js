window.addEventListener('DOMContentLoaded', async () => {
  
    const url = 'http://localhost:8000/api/conferences/';
    try{
      const response = await fetch(url);
      if(!response.ok){
      // console.log(response);
          throw new Error('Response not ok');
      }else{
          const formTag = document.getElementById('create-attendee-form');
          formTag.addEventListener('submit', async event => {
              event.preventDefault();
              const formData = new FormData(formTag);
              const json = JSON.stringify(Object.fromEntries(formData));
              console.log(json);

              const locationUrl = 'http://localhost:8001/api/attendees/';
              const fetchConfig = {
              method: "post",
              body: json,
              headers: {
                  'Content-Type': 'application/json',
              },
              };
              const response = await fetch(locationUrl, fetchConfig);
              console.log (response)
              if (response.ok) {
              formTag.reset();
              const newLocation = await response.json();
              console.log(newLocation);
              const success = document.getElementById('success-message');
              success.classList.remove('d-none')
              formTag.classList.add('d-none')
              }

      }); 
          
        const selectTag = document.getElementById('conference');
        const data = await response.json();
        console.log(data);
        for (let conference of data.conferences) {
          const option = document.createElement('option');
          option.value = conference.href;
          option.innerHTML = conference.name;
          selectTag.appendChild(option);
        }
        const spinner = document.getElementById('loading-conference-spinner');
        spinner.classList.add('d-none')

        selectTag.classList.remove('d-none')
          }
      

  

  }catch (error) {
      console.error('error', error);
  }
});


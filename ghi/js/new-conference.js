window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    try{
        const response = await fetch(url);
        if(!response.ok){
        // console.log(response);
            throw new Error('Response not ok');
        }else{
            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                console.log(json);

                const locationUrl = 'http://localhost:8000/api/conferences/';
                const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
                };
                const response = await fetch(locationUrl, fetchConfig);
                console.log (response)
                if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
                console.log(newLocation);

                }

        }); 
            
            
            const categorySelect=document.getElementById("location")
            const data = await response.json();
            console.log(data);
            for (let location of data.locations){
                const option= document.createElement('option');
                option.innerHTML=location.name
                option.value=location.id
                categorySelect.appendChild(option)
            }
        
        }
    

    }catch (error) {
        console.error('error', error);
    }
});

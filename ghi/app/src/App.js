import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import NewConference from './new-conference';
import Attend from './attend-conference';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <Nav />
      <div className="container">
      <LocationForm />
      <AttendeesList attendees={props.attendees} />
      </div>
      <div className='container'>
        <NewConference/>
        <Attend/>
      </div>
      
    </>
  );
}

export default App;
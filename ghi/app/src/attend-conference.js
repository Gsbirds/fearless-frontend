import React, { useEffect, useState } from "react";
function Attend() {
  const [conferences, setConferences] = useState([]);
  const [conference, setConference] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  

  let spinnerClasses = "d-flex justify-content-center mb-3";
  let dropdownClasses = "form-select d-none";
  if (conferences.length > 0) {
    spinnerClasses = "d-flex justify-content-center mb-3 d-none";
    dropdownClasses = "form-select";
  }

  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };
  const handleEmail = (event) => {
    const value = event.target.value;
    setEmail(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    // create an empty JSON object
    const data = {};

    data.conference = conference;
    data.name = name;
    data.email = email;

    console.log(data);

    const locationUrl = "http://localhost:8001/api/attendees/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);
      setName("");
      setConference("");
      setEmail("");
    }
  };
  const fetchData = async () => {
    const url = "http://localhost:8000/api/conferences/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
      console.log(data);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="container">
      <div className="my-5">
        <div className="row">
          <div className="col col-sm-auto">
            <img
              width="300"
              class="bg-white rounded shadow d-block mx-auto mb-4"
              src="/logo.svg"
            />
          </div>
          <div className="col">
            <div className="card shadow">
              <div className="card-body">
                <form onSubmit={handleSubmit} id="create-location-form">
                  <h1 className="card-title">It's Conference Time!</h1>
                  <p className="mb-3">
                    Please choose which conference you'd like to attend.
                  </p>
         
                  <div
                    className={spinnerClasses}
                    id="loading-conference-spinner"
                  >
                    <div className="spinner-grow text-secondary" role="status">
                      <span className="visually-hidden">Loading...</span>
                    </div>
                  </div>
                  <div className="mb-3">
                    <select
                      className={dropdownClasses}
                      onChange={handleConferenceChange}
                      name="conference"
                      id="conference"
                      // className="form-select "
                      required
                      value={conference}
                    >
                      <option value="">Choose a conference</option>
                      {conferences.map((conference) => {
                        return (
                          <option key={conference.href} value={conference.href}>
                            {conference.name}
                          </option>
                        );
                      })}
                    </select>
                  </div>
                  <p className="mb-3">Now, tell us about yourself.</p>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input
                          onChange={handleNameChange}
                          required
                          placeholder="Your full name"
                          type="text"
                          id="name"
                          name="name"
                          className="form-control"
                          value={name}
                        />
                        <label htmlfor="name">Your full name</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input
                          onChange={handleEmail}
                          required
                          placeholder="Your email address"
                          type="email"
                          id="email"
                          name="email"
                          className="form-control"
                          value={email}
                        />
                        <label htmlfor="email">Your email address</label>
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-lg btn-primary">I'm going!</button>
                </form>
                <div
                  className="alert alert-success d-none mb-0"
                  id="success-message"
                >
                  Congratulations! You're all signed up!
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Attend;

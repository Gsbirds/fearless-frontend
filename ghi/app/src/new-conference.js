import React, { useEffect, useState } from "react";

function NewConference(){
    const [locations, setLocations] = useState([]);
    const [location, setLocation]= useState('')
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [max_presentations, setPresentations] = useState('');
    const [max_attendees, setAttendees] = useState('');
  
    const handleLocationChange = (event) => {
      const value = event.target.value;
      setLocation(value);
    }
  
    const handleNameChange = (event) => {
      const value = event.target.value;
      setName(value);
    }
    const handleStarts = (event) => {
      const value = event.target.value;
      setStarts(value);
    }
    const handleEnds = (event) => {
      const value = event.target.value;
      setEnds(value);
    }
    const handleDescription = (event) => {
        const value = event.target.value;
        setDescription(value);
      }
    const handlePresentations = (event) => {
        const value = event.target.value;
        setPresentations(value);
      }
      const handleAttendees = (event) => {
        const value = event.target.value;
        setAttendees(value);
      }
    const handleSubmit = async (event) => {
      event.preventDefault();
    
      // create an empty JSON object
      const data = {};
    
     
      data.name = name;
      data.description= description;
      data.starts = starts;
      data.ends = ends;
      data.max_presentations= max_presentations;
      data.max_attendees=max_attendees;
      data.location = location;
    
      console.log(data);
    
  
    const locationUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
  
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);
      setName('');
      setStarts('');
      setEnds('');
      setLocation('');
      setDescription('');
      setPresentations('');
      setAttendees('');
    }
  
  }
    const fetchData = async () => {
      const url = "http://localhost:8000/api/locations/";
  
      const response = await fetch(url);
  
      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations)
        console.log(data)
      }
    };
  
    useEffect(() => {
      fetchData();
    }, []);
  
  

return(

<div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" value={name} required type="text" id="name" name="name" className="form-control"/>
                <label htmlfor="name">Name</label>
              </div>
              
              <div className="form-floating mb-3">
                <input onChange={handleStarts} placeholder="Starts" value={starts} required type="date" id="starts" name="starts" className="form-control"/>
                <label htmlfor="name">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEnds} placeholder="Ends" value ={ends}required type="date" id="ends" name="ends" className="form-control"/>
                <label htmlfor="name">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlfor="exampleFormControlInput1" className="form-label">Description</label>
                <textarea onChange={handleDescription} className="form-control" id="description" name= "description" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresentations} value= {max_presentations} placeholder="Maximum presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control"/>
                <label htmlfor="room_count">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleAttendees} value= {max_attendees} placeholder="Maximum attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control"/>
                <label htmlfor="room_count">Maximum Attendees</label>
              </div>
              <div className="mb-3">
              <select onChange={handleLocationChange} value= {location} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>{location.name}</option>
                  );
                })}
              </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
)
      }

      export default NewConference;